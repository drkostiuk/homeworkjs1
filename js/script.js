/*1. Змінну можна оголосити за допомогою ключового слова  let, або const.
При оголошенні змінної з ключовим словом let, змінній також можна присвоїти 
значення будь-якого типу даних. 

При оголошенні змінної з ключовим словом const
 і їй можна присвоїти значення тільки один раз. Після того, як значення було присвоєне змінній,
воно не може бути змінене. 
2.prompt() дозволяє відобразити діалогове вікно, в якому користувач може ввести текстове значення.
 prompt()  поверне введене користувачем значення або null, якщо користувач вибрав "відмінити".
 confirm() дозволяє відобразити діалогове вікно з запитанням, на яке користувач може відповісти 
"так" або "ні". Функція поверне true, якщо користувач вибрав "так", і false, якщо користувач вибрав "ні".
3. Неявне перетворення типів (implicit type coercion) - це автоматичне перетворення типу даних з одного 
типу в інший без явного вказування цього процесу у коді програми.
 Наприклад, якщо ми додаємо рядок і число у JavaScript, то число автоматично перетворюється в рядок 
 перед тим як відбутися конкатенація:
наприклад
let str = "10";
let num = 5;
let result = str + num; // неявне перетворення типу: num автоматично конвертується в рядок
console.log(result); // "105"*/
let admin;
let name = "Yuliia";
admin = name;
console.log(admin);

const days = 10;
console.log(days * 24 * 60 * 60);
console.log(prompt("Введіть значення"));
